def create_key(method, field)
  case method
    when 'clean'
      "#{field}_cleaned_name".to_sym
    when 'normal'
      "#{field}_normalized_name".to_sym
    when 'original'
      "#{field}_original_name".to_sym
  end
end

def create_hash(method, field, val)
  { create_key(method, field) => val }
end

def create_art_key(method)
  case method
    when 'clean'
      :art_cleaned_names
    when 'normal'
      :art_normalized_names
    when 'original'
      :art_original_names
  end
end

def create_art_hash(method, val)
  { create_art_key(method) => val }
end

def create_query(method, tck_title,album_name,art_names)
  FlatNormalName.where(normalized_type: tck)
      .where(create_hash(method, 'tck', tck_title))
      .where(create_hash(method, 'album', album_name))
      .in(create_art_hash(method, art_names))
end

def normalize(fields)
  tck_normalized_name = MXText::Normalizer.normalize_tck_title(fields[:tck_title])
  album_normalized_name = MXText::Normalizer.normalize_album_title(fields[:album_name])
  art_normalized_names = fields[:art_names_arr].map {|name| MXText::Normalizer.normalize_art_name(name)}

  create_query('normal', tck_normalized_name, album_normalized_name, art_normalized_names)
end

def clean(fields)
  tck_clean_name = MXText::Cleaner.clean_tck_title(fields[:tck_title])
  album_clean_name = MXText::Cleaner.clean_album_title(fields[:album_name])
  art_clean_names = fields[:art_names_arr].map {|name| MXText::Cleaner.clean_art_name(name)}

  create_query('clean', tck_clean_name, album_clean_name, art_clean_names)
end

def original(fields)
  create_query('original', fields[:tck_title], fields[:album_name], fields[:art_names_arr])
end

def tcks_found?(query)
  tck_id = query.distinct(:normalized_id)
  tck.active.in(id: tck_id).count > 0 ? true : false
end

def query_build(method, fields)
  case method
  when 'normal'
      normalize(fields)
    when 'clean'
      clean(fields)
    when 'original'
      original(fields)
  end
end

def get_tck_info(query)
  tck_mn_id = query.pluck(:mn_id).uniq
  tck_id = query.map {|fnn| tck.where(mn_id: fnn.mn_id).first.sid }.uniq
  lo_name = query.map {|fnn| tck.where(mn_id: fnn.mn_id).try(:first).try(:Lab).try(:name) }.uniq

  {
      tck_mn_id: tck_mn_id.size > 1 ? 'Multiple results' : tck_mn_id.first,
      tck_id: tck_id.size > 1 ? 'Multiple results' : tck_id.first,
      lo_name: lo_name.size > 1 ? 'Multiple results' : lo_name.first
  }
end

CSV.open('/tmp/tck_existence.csv', 'w', headers: true) do |line|
  line << ['Song Title', 'art name', 'tck MN ID', 'D Id', 'Lab Owner name']

  CSV.foreach('/tmp/xlsx_csv.csv', headers: true) do |row|
    tck_title = row[1]
    art_names = row[2]
    album_name = row[3]

    art_names_arr = art_names.split('\;').map(&:strip)

    query = query_build('original', {tck_title: tck_title, album_name: album_name, art_names_arr: art_names_arr})
    if tcks_found?(query)
      tck_info = get_tck_info(query)

      line << [tck_title, art_names_arr.join(' | '), tck_info[:tck_mn_id], tck_info[:tck_id], tck_info[:lo_name]]
      next
    end

    query = query_build('normal', {tck_title: tck_title, album_name: album_name, art_names_arr: art_names_arr})
    if tcks_found?(query)
      tck_info = get_tck_info(query)

      line << [tck_title, art_names_arr.join(' | '), tck_info[:tck_mn_id], tck_info[:tck_id], tck_info[:lo_name]]
      next
    end

    query = query_build('clean', {tck_title: tck_title, album_name: album_name, art_names_arr: art_names_arr})
    if tcks_found?(query)
      tck_info = get_tck_info(query)

      line << [tck_title, art_names_arr.join(' | '), tck_info[:tck_mn_id], tck_info[:tck_id], tck_info[:lo_name]]
    end
  end
end
