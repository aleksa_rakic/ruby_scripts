namespace :generate do
  desc 'creates CSV report for lab Owners'
  task :lo_report => :environment do
    generate_csv('labOwner')
  end

  desc 'creates CSV report for lab Owner Organizations'
  task :lo_organizations_report, [:status] => :environment do |_t, args|
    generate_csv('labOwnerOrganization', args[:status])
  end

  desc 'creates CSV report for labs'
  task :lab_report => :environment do
    generate_csv('lab')
  end

  desc 'creates CSV report for lab Organizations'
  task :lab_organizations_report, [:status] => :environment do |_t, args|
    generate_csv('labOrganization', args[:status])
  end
end

def create_header_and_footer(klass, status)
  status_message = status.nil? ? 'all' : status

  case klass
    when 'labOwner'
      {
          header: %w(lo_name lab_count tck_count),
          footer: ['Grand total:', "lab owners total count: #{labOwner.count}", "Active tcks total count: #{tck.active.count}"]
      }
    when 'labOwnerOrganization'
      status_criteria = create_status_criteria(status)
      {
          header:  %w(lo_mn_id lo_name lo_org_name lab_count artists_count albums_count tck_count status),
          footer: ['Grand total:', "lab owner organizations total count: #{labOwnerOrganization.in(status_criteria).count}", "Active tcks total count: #{tck.active.count}", status_message]
      }
    when 'lab'
      {
          header: %w(lab_name tck_count),
          footer: ['Grand total:', "labs total count: #{lab.count}", "Active tcks total count: #{tck.active.count}"]
      }
    when 'labOrganization'
      status_criteria = create_status_criteria(status)
      {
          header:  %w(lab_mn_id lab_name lab_org_name artists_count albums_count tck_count status),
          footer: ['Grand total:', "lab organizations total count: #{labOrganization.in(status_criteria).count}", "Active tcks total count: #{tck.active.count}", status_message]
      }
  end
end

def create_data_array(entity)
  case entity.class
    when labOwner
      [entity.name, entity.labs.count, entity.tcks.count]
    when labOwnerOrganization
      rights_holder_data = get_rights_holder(entity)
      catalog = entity.catalog_count_calculation
      [rights_holder_data[:rh_mn_id], rights_holder_data[:rh_name], entity.name, catalog[:labs], catalog[:artists_count], catalog[:albums], catalog[:tcks], entity.status]
    when lab
      [entity.name, entity.tcks.count]
    when labOrganization
      rights_holder_data = get_rights_holder(entity)
      catalog = entity.catalog_count_calculation
      [rights_holder_data[:rh_mn_id], rights_holder_data[:rh_name], entity.name, catalog[:artists_count], catalog[:albums], catalog[:tcks], entity.status]
  end
end

def create_status_criteria(status)
  status.present? ? {status: [status]} : {status: ['active', 'inactive', 'deleted']}
end

def create_query(klass, status)
  case klass
    when 'labOwner'
      labOwner.all
    when 'labOwnerOrganization'
      status_criteria = create_status_criteria(status)
      labOwnerOrganization.in(status_criteria)
    when 'lab'
      lab.all
    when 'labOrganization'
      status_criteria = create_status_criteria(status)
      labOrganization.in(status_criteria)
  end
end

def get_rights_holder(entity)
  rights_holder = entity.rights_holder

  if rights_holder
    {rh_mn_id: rights_holder.mn_id, rh_name: rights_holder.name}
  else
    {rh_mn_id: '-', rh_name: '-'}
  end
end

def generate_path(klass, status)
  datestamp = DateTime.now.strftime('%Y%m%d') #%H%M%S
  klass = klass.split(/(?=[A-Z])/).map(&:downcase).join('_')
  path = "/tmp/#{klass}_report_for_#{datestamp}"
  path += "_status_#{status}" if status
  path + '.csv'
end

def generate_csv(klass, status=nil)
  report_path = generate_path(klass, status)
  query = create_query(klass, status)
  array = create_header_and_footer(klass, status)

  CSV.open(report_path, 'w:UTF-8', force_quotes: true, headers: true) do |row|
    row << array[:header]

    query.no_timeout.each do |entity|
      row << create_data_array(entity)
    end

    row << array[:footer]
  end

  puts "+report is saved in '#{report_path}' file."
end
